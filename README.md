# Hopper

* Create a mysql service
```shell script
docker network create --driver overlay --attachable cenpay

docker service create \
  --env MYSQL_ROOT_PASSWORD=mysql \
  --network cenpay \
  --publish published=3306,target=3306 \
  --mount type=volume,source=mysql,destination=/var/lib/mysql \
  --name mysql mysql:8.0 


docker service create \
  --network cenpay \
  --env MONGO_INITDB_ROOT_USERNAME=root \
  --env MONGO_INITDB_ROOT_PASSWORD=mongo \
  --publish published=27017,target=27017 \
  --mount type=volume,source=mongodb,destination=/data/db \
  --name mongodb mongo:latest
```