// Package registration represents the concrete implementation of ApiKeyCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package authuc

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/hopper/auth"
	"gitlab.com/zenpagos/hopper/usecase"
)

type AuthUseCase struct {
	AuthorizationManager auth.AuthorizationManagerInterface
}

func NewAuthUseCase(am auth.AuthorizationManagerInterface) usecase.AuthUseCaseInterface {
	return AuthUseCase{
		AuthorizationManager: am,
	}
}

func (uc AuthUseCase) CreateRolesAndPermissions(accountID uuid.UUID) error {
	return uc.AuthorizationManager.CreateRolesAndPermissions(accountID)
}

func (uc AuthUseCase) AttachOwnerRole(actorID uuid.UUID, accountID uuid.UUID) error {
	return uc.AuthorizationManager.AttachRole(actorID, accountID, auth.OwnerRole)
}

func (uc AuthUseCase) AttachAdminRole(actorID uuid.UUID, accountID uuid.UUID) error {
	return uc.AuthorizationManager.AttachRole(actorID, accountID, auth.AdminRole)
}

func (uc AuthUseCase) Authorize(actorID uuid.UUID, accountID uuid.UUID, resource string, action string) (bool, error) {
	return uc.AuthorizationManager.Authorize(actorID, accountID, resource, action)
}
