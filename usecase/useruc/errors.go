package useruc

import "errors"

var (
	ErrUserAlreadyExist       = errors.New("user exists")
	ErrUserRegistration       = errors.New("is not possible register the user")
	ErrInvalidCredentials     = errors.New("invalid login credentials. Please try again")
	ErrUnauthorizedUser       = errors.New("unauthorized user")
	ErrUnableToCreatePolicies = errors.New("unable to create policies")
)
