// Package registration represents the concrete implementation of UserCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package useruc

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/hopper/auth"
	"gitlab.com/zenpagos/hopper/config"
	"gitlab.com/zenpagos/hopper/model"
	"gitlab.com/zenpagos/hopper/repository"
	"gitlab.com/zenpagos/hopper/usecase"
	"gitlab.com/zenpagos/hopper/wrapper/wkafka"
	"gitlab.com/zenpagos/robin/schema"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type UserUseCase struct {
	Configuration        config.Configuration
	WKafka               *wkafka.KafkaWrapper
	AuthorizationManager auth.AuthorizationManagerInterface
	UserRepository       repository.UserRepositoryInterface
}

func NewUserUseCase(
	conf config.Configuration,
	kw *wkafka.KafkaWrapper,
	am auth.AuthorizationManagerInterface,
	ar repository.UserRepositoryInterface,
) usecase.UserUseCaseInterface {
	return UserUseCase{
		Configuration:        conf,
		WKafka:               kw,
		AuthorizationManager: am,
		UserRepository:       ar,
	}
}

func (uc UserUseCase) CreateUser(u *model.User) error {
	if err := u.Validate(); err != nil {
		return err
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), 8)
	if err != nil {
		log.WithError(err).Error()
		return ErrUserRegistration
	}

	u.Password = string(hashedPassword)

	unique, err := uc.UserRepository.IsUnique(u)
	if unique == false {
		log.WithError(err).Error()
		return ErrUserAlreadyExist
	}

	if err := uc.UserRepository.Insert(u); err != nil {
		log.WithError(err).Error()
		return ErrUserRegistration
	}

	m := schema.UserRegisteredSchema{
		FirstName: u.FirstName,
		LastName:  u.LastName,
		Email:     u.Email,
	}
	msg, err := json.Marshal(m)
	if err != nil {
		// dont return the error to dont
		// break the registration process
		log.WithError(err).Error()
	}
	if err := uc.WKafka.PutMessage(config.UserRegisteredTopic, msg); err != nil {
		// dont return the error to dont
		// break the registration process
		log.WithError(err).Error()
	}

	return nil
}

func (uc UserUseCase) verifyAccessToken(at string) (*model.Claims, error) {
	claims := &model.Claims{}
	s := uc.Configuration.Service.Secret

	tkn, err := jwt.ParseWithClaims(at, claims, func(tk *jwt.Token) (interface{}, error) {
		return []byte(s), nil
	})
	if err != nil || !tkn.Valid {
		log.WithError(err).Error()
		return nil, ErrUnauthorizedUser
	}

	return claims, nil
}

func (uc UserUseCase) FindByAccessToken(accessToken string) (*model.User, error) {
	claims, err := uc.verifyAccessToken(accessToken)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	// retrieve the user from database to check if exists
	claimUserID := claims.ID.String()
	return uc.UserRepository.FindOne(claimUserID)
}

func (uc UserUseCase) Authenticate(e string, p string) (*string, error) {
	user, err := uc.UserRepository.FindOneByEmail(e)
	if err != nil {
		return nil, ErrInvalidCredentials
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(p))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		log.WithError(err).Error()
		return nil, ErrInvalidCredentials
	}

	expiresAt := time.Now().Add(time.Minute * 100000).Unix()
	claims := &model.Claims{
		ID:        user.ID,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secret := uc.Configuration.Service.Secret
	token, err := at.SignedString([]byte(secret))
	if err != nil {
		log.WithError(err).Error()
		return nil, ErrInvalidCredentials
	}

	return &token, nil
}
