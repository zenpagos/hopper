// Package registration represents the concrete implementation of ApiKeyCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package apikeyuc

import (
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/hopper/auth"
	"gitlab.com/zenpagos/hopper/config"
	"gitlab.com/zenpagos/hopper/model"
	"gitlab.com/zenpagos/hopper/repository"
	"gitlab.com/zenpagos/hopper/usecase"
	"gitlab.com/zenpagos/tools"
)

type ApiKeyUseCase struct {
	ApiKeyRepository repository.ApiKeyRepositoryInterface
}

func NewApiKeyUseCase(apr repository.ApiKeyRepositoryInterface) usecase.ApiKeyUseCaseInterface {
	return ApiKeyUseCase{
		ApiKeyRepository: apr,
	}
}

func (uc ApiKeyUseCase) FindByToken(t string) (*model.ApiKey, error) {
	return uc.ApiKeyRepository.FindOneByToken(t)
}

func (uc ApiKeyUseCase) CreateApiKey(ak *model.ApiKey) error {
	if err := ak.Validate(); err != nil {
		return err
	}

	token, err := tools.GenerateTokenURLSafe(config.ApiKeyLength)
	if err != nil {
		log.WithError(err).Error()
		return ErrUnableToCreateApiKey
	}

	ak.Token = token
	ak.Role = auth.AdminRole
	ak.Status = model.ActiveApiKeyStatus

	if err := uc.ApiKeyRepository.Insert(ak); err != nil {
		log.WithError(err).Error()
		return ErrUnableToCreateApiKey
	}

	return nil
}

func (uc ApiKeyUseCase) ListApiKeys(accountID uuid.UUID) ([]model.ApiKey, error) {
	return uc.ApiKeyRepository.FindByAccountID(accountID)
}
