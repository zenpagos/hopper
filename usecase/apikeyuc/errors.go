package apikeyuc

import "errors"

var (
	ErrUnableToCreateApiKey = errors.New("unable to create api key")
)
