// Package usecase defines all the interfaces for a Micro-service application.
// It is the entry point for the application's business logic. It is a top level package for a Micro-service application.
// This top level package only defines interface, the concrete implementations are defined in sub-package of it.
// It only depends on model package. No other package should dependent on it except cmd.

// If transaction is supported, the transaction boundary should be defined in this package.
// A suffix-"WithTx" can be added to the name of a transaction function to distinguish it from a non-transaction one.
package usecase

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/hopper/model"
)

type UserUseCaseInterface interface {
	CreateUser(user *model.User) error
	FindByAccessToken(accessToken string) (*model.User, error)
	Authenticate(email string, password string) (*string, error)
}

type ApiKeyUseCaseInterface interface {
	CreateApiKey(apiKey *model.ApiKey) error
	FindByToken(token string) (*model.ApiKey, error)
	ListApiKeys(accountID uuid.UUID) ([]model.ApiKey, error)
}

type AuthUseCaseInterface interface {
	CreateRolesAndPermissions(accountID uuid.UUID) error
	AttachOwnerRole(actorID uuid.UUID, accountID uuid.UUID) error
	AttachAdminRole(actorID uuid.UUID, accountID uuid.UUID) error
	Authorize(actorID uuid.UUID, accountID uuid.UUID, resource string, action string) (bool, error)
}
