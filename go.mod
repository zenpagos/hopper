module gitlab.com/zenpagos/hopper

go 1.14

require (
	github.com/casbin/casbin/v2 v2.2.2
	github.com/casbin/gorm-adapter/v2 v2.1.0
	github.com/confluentinc/confluent-kafka-go v1.4.1-0.20200429183238-69b368320407
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-ozzo/ozzo-validation/v4 v4.2.1
	github.com/golang/protobuf v1.3.5
	github.com/google/wire v0.4.0
	github.com/jinzhu/gorm v1.9.12
	github.com/jnewmano/grpc-json-proxy v0.0.0-20200427184142-6696b5a3ab05
	github.com/lib/pq v1.5.2 // indirect
	github.com/mitchellh/mapstructure v1.3.0 // indirect
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.3
	gitlab.com/zenpagos/robin v0.0.0-20200616233949-167822b2aed9
	gitlab.com/zenpagos/tools v0.0.0-20200616225453-f02e1e8b6545
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
	google.golang.org/grpc v1.28.1
	gopkg.in/ini.v1 v1.55.0 // indirect
)
