#!/bin/bash

SVC_DEPLOYMENT=hopper

docker build -t 127.0.0.1:5000/hopper .

docker push 127.0.0.1:5000/hopper

helm upgrade --install \
  -f scripts/hopper-values.yaml \
  ${SVC_DEPLOYMENT} kubernetes/hopper
