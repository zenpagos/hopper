package igrpc

import (
	"context"
	_ "github.com/jnewmano/grpc-json-proxy/codec"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/hopper/container"
	"gitlab.com/zenpagos/hopper/model"
	"gitlab.com/zenpagos/hopper/proto/v1"
	"gitlab.com/zenpagos/tools"
)

type gRPCServer struct {
	container container.Container
}

func NewGRPCServer(c container.Container) pbv1.HopperServiceServer {
	return &gRPCServer{
		container: c,
	}
}

func (s *gRPCServer) CreateUser(
	c context.Context,
	req *pbv1.CreateUserRequest,
) (*pbv1.CreateUserResponse, error) {

	u := &model.User{
		FirstName:         req.FirstName,
		LastName:          req.LastName,
		Email:             req.Email,
		Password:          req.Password,
		MobilePhoneNumber: req.MobilePhoneNumber,
	}

	if err := s.container.UserUseCase.CreateUser(u); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	res := &pbv1.CreateUserResponse{
		ID:                u.ID.String(),
		CreatedAt:         u.CreatedAt,
		FirstName:         u.FirstName,
		LastName:          u.LastName,
		Email:             u.Email,
		MobilePhoneNumber: u.MobilePhoneNumber,
	}

	return res, nil
}

func (s *gRPCServer) AttachFullAccessPolicies(
	c context.Context,
	req *pbv1.AttachFullAccessPoliciesRequest,
) (*pbv1.AttachFullAccessPoliciesResponse, error) {

	accessToken := req.AccessToken
	accountID := uuid.FromStringOrNil(req.AccountID)

	u, err := s.container.UserUseCase.FindByAccessToken(accessToken)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	if err := s.container.AuthUseCase.CreateRolesAndPermissions(accountID); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	if err := s.container.AuthUseCase.AttachOwnerRole(u.ID, accountID); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	return &pbv1.AttachFullAccessPoliciesResponse{}, nil
}

func (s *gRPCServer) Authenticate(
	c context.Context,
	req *pbv1.AuthenticateRequest,
) (*pbv1.AuthenticateResponse, error) {

	jwt, err := s.container.UserUseCase.Authenticate(req.Email, req.Password)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	res := &pbv1.AuthenticateResponse{
		AccessToken: *jwt,
	}

	return res, nil
}

func (s *gRPCServer) AuthorizeByAnAccessToken(
	c context.Context,
	req *pbv1.AuthorizeByAnAccessTokenRequest,
) (*pbv1.AuthorizeByAnAccessTokenResponse, error) {

	accessToken := req.AccessToken
	resource := req.Resource
	action := req.Action
	accountID := uuid.FromStringOrNil(req.AccountID)

	u, err := s.container.UserUseCase.FindByAccessToken(accessToken)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	result, err := s.container.AuthUseCase.Authorize(u.ID, accountID, resource, action)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	res := &pbv1.AuthorizeByAnAccessTokenResponse{
		Result: result,
	}

	return res, nil
}

func (s *gRPCServer) AuthorizeByAnApiKey(
	c context.Context,
	req *pbv1.AuthorizeByAnApiKeyRequest,
) (*pbv1.AuthorizeByAnApiKeyResponse, error) {

	token := req.ApiKey
	resource := req.Resource
	action := req.Action
	accountID := uuid.FromStringOrNil(req.AccountID)

	ak, err := s.container.ApiKeyUseCase.FindByToken(token)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	result, err := s.container.AuthUseCase.Authorize(ak.ID, accountID, resource, action)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	res := &pbv1.AuthorizeByAnApiKeyResponse{
		Result: result,
	}

	return res, nil
}

func (s *gRPCServer) CreateApiKey(
	c context.Context,
	req *pbv1.CreateApiKeyRequest,
) (*pbv1.CreateApiKeyResponse, error) {

	accessToken := req.AccessToken
	accountID := uuid.FromStringOrNil(req.AccountID)

	// TODO: here we MUST TO CHECK if the accountID can be used by the access token owner,
	// it must not be able to create API keys for another account!!!

	u, err := s.container.UserUseCase.FindByAccessToken(accessToken)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	ak := &model.ApiKey{
		Name:      req.Name,
		UserID:    u.ID,
		AccountID: accountID,
	}

	if err := s.container.ApiKeyUseCase.CreateApiKey(ak); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	if err := s.container.AuthUseCase.AttachAdminRole(ak.ID, accountID); err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	akr := &pbv1.ApiKey{
		ID:        ak.ID.String(),
		CreatedAt: ak.CreatedAt,
		UpdatedAt: tools.Int64ValueOrZero(ak.UpdatedAt),
		Name:      ak.Name,
		Status:    ak.Status,
		Role:      ak.Role,
		Token:     ak.Token,
	}

	res := &pbv1.CreateApiKeyResponse{
		ApiKey: akr,
	}

	return res, nil
}

func (s *gRPCServer) ListApiKeys(
	c context.Context,
	req *pbv1.ListApiKeysRequest,
) (*pbv1.ListApiKeysResponse, error) {

	accountID := uuid.FromStringOrNil(req.AccountID)

	aks, err := s.container.ApiKeyUseCase.ListApiKeys(accountID)
	if err != nil {
		return nil, err
	}

	apiKeys := make([]*pbv1.ApiKey, 0)
	for _, ak := range aks {
		apiKeys = append(apiKeys, &pbv1.ApiKey{
			ID:        ak.ID.String(),
			CreatedAt: ak.CreatedAt,
			UpdatedAt: tools.Int64ValueOrZero(ak.UpdatedAt),
			Name:      ak.Name,
			Status:    ak.Status,
			Role:      ak.Role,
			Token:     ak.Token,
		})
	}

	res := &pbv1.ListApiKeysResponse{
		ApiKeys: apiKeys,
	}

	return res, nil
}
