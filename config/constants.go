package config

const (
	UserRegisteredTopic = "notification.user.registered"

	ApiKeyLength = 64
)
