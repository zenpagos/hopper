package config

type ServiceConfiguration struct {
	Port   int    `mapstructure:"port"`
	Prod   bool   `mapstructure:"prod"`
	Secret string `mapstructure:"secret"`
}
