package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/hopper/auth"
	"gitlab.com/zenpagos/hopper/config"
	"gitlab.com/zenpagos/hopper/container"
	"gitlab.com/zenpagos/hopper/interface/igrpc"
	"gitlab.com/zenpagos/hopper/proto/v1"
	"gitlab.com/zenpagos/hopper/repository/mysqlrepo"
	"gitlab.com/zenpagos/hopper/wrapper/wkafka"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
)

func init() {
	log.SetReportCaller(true)
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.JSONFormatter{
		PrettyPrint: true,
	})
}

func main() {
	// read configuration
	conf, err := config.ReadConfig()
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}
	defer db.Close()

	// create kafka wrapper
	kw, err := wkafka.NewKafkaWrapper(conf)
	if err != nil {
		log.Panicf("error creating kafka wrapper: %v", err)
	}
	defer kw.Producer.Close()

	// create authorization manager
	am, err := auth.NewAuthorizationManager(db)
	if err != nil {
		log.Panicf("failed to create casbin enforcer: %v", err)
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", conf.Service.Port))
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	c := container.InitializeContainer(db, kw, am, conf)

	s := grpc.NewServer()

	svc := igrpc.NewGRPCServer(c)
	pbv1.RegisterHopperServiceServer(s, svc)

	healthService := igrpc.NewHealthChecker()
	grpc_health_v1.RegisterHealthServer(s, healthService)

	if err := s.Serve(lis); err != nil {
		log.Panicf("failed to serve: %v", err)
	}
}
