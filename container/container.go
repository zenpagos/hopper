package container

import (
	"gitlab.com/zenpagos/hopper/usecase"
)

type Container struct {
	UserUseCase   usecase.UserUseCaseInterface
	ApiKeyUseCase usecase.ApiKeyUseCaseInterface
	AuthUseCase   usecase.AuthUseCaseInterface
}

func NewContainer(
	uuc usecase.UserUseCaseInterface,
	akuc usecase.ApiKeyUseCaseInterface,
	auc usecase.AuthUseCaseInterface,
) Container {
	return Container{
		UserUseCase:   uuc,
		ApiKeyUseCase: akuc,
		AuthUseCase:   auc,
	}
}
