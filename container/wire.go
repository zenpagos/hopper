//+build wireinject

package container

import (
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/hopper/auth"
	"gitlab.com/zenpagos/hopper/config"
	"gitlab.com/zenpagos/hopper/repository/mysqlrepo"
	"gitlab.com/zenpagos/hopper/usecase/apikeyuc"
	"gitlab.com/zenpagos/hopper/usecase/authuc"
	"gitlab.com/zenpagos/hopper/usecase/useruc"
	"gitlab.com/zenpagos/hopper/wrapper/wkafka"
)

func InitializeContainer(
	db *gorm.DB,
	kw *wkafka.KafkaWrapper,
	am auth.AuthorizationManagerInterface,
	conf config.Configuration,
) Container {

	wire.Build(
		// Repositories
		mysqlrepo.NewUserRepository,
		mysqlrepo.NewApiKeyRepository,
		// Use Cases
		useruc.NewUserUseCase,
		apikeyuc.NewApiKeyUseCase,
		authuc.NewAuthUseCase,
		NewContainer,
	)

	return Container{}
}
