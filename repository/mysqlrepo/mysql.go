package mysqlrepo

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/hopper/config"
	"gitlab.com/zenpagos/hopper/model"
	"gitlab.com/zenpagos/tools"
)

func NewMysqlClient(c config.Configuration) (*gorm.DB, error) {
	connectionUri := fmt.Sprintf(
		"%s:%s@(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		c.Database.Username,
		c.Database.Password,
		c.Database.Host,
		c.Database.Name,
	)

	db, err := gorm.Open("mysql", connectionUri)
	if err != nil {
		log.Fatalf("error open connection: %s", err)
		return nil, err
	}

	db.Callback().
		Create().
		Before("gorm:create").
		After("gorm:before_create").
		Register("generateUUID:before_create", generateUUID)

	db.Callback().
		Create().
		Replace("gorm:update_time_stamp", updateTimeStampForCreateCallback)

	db.Callback().
		Update().
		Replace("gorm:update_time_stamp", updateTimeStampForUpdateCallback)

	if c.Service.Prod == false {
		if err := createFixtures(db); err != nil {
			log.Fatalf("error creating fixtures: %s", err)
			return nil, err
		}
	}

	return db, nil
}

func generateUUID(scope *gorm.Scope) {
	if !scope.HasError() {
		id := uuid.NewV4()

		if IDField, ok := scope.FieldByName("ID"); ok {
			if IDField.IsBlank {
				IDField.Set(id)
			}
		}
	}
}

func updateTimeStampForCreateCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		now := tools.NewUnixTime()

		if createdAtField, ok := scope.FieldByName("CreatedAt"); ok {
			if createdAtField.IsBlank {
				createdAtField.Set(now)
			}
		}
	}
}

func updateTimeStampForUpdateCallback(scope *gorm.Scope) {
	if _, ok := scope.Get("gorm:update_column"); !ok {
		now := tools.NewUnixTime()
		scope.SetColumn("UpdatedAt", now)
	}
}

func createFixtures(db *gorm.DB) error {
	db.LogMode(true)
	db.DropTableIfExists(
		&model.User{},
		&model.ApiKey{},
	)
	db.AutoMigrate(
		&model.User{},
		&model.ApiKey{},
	)

	users := []model.User{
		{
			FirstName: "Jesus",
			LastName:  "Diaz",
			Email:     "jesusdiazbc@gmail.com",
			Password:  "$2a$08$nQT5IP1PHjqAgCm5d6fn8.iv1hGQzgzLqQhyKli2zg5epKApq6WX.", // demo
		},
		{
			FirstName: "Monica",
			LastName:  "Galarce",
			Email:     "monikagalarce@gmail.com",
			Password:  "$2a$08$HxS6saWFIN/Wi99jfFcECOnnSnKk4OvxaOZM5dAygPavO2oST/Cju", // demo
		},
	}

	userRepo := NewUserRepository(db)
	for _, u := range users {
		if err := userRepo.Insert(&u); err != nil {
			return err
		}
	}

	return nil
}
