package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/hopper/model"
	"gitlab.com/zenpagos/hopper/repository"
)

type UserRepository struct {
	DB *gorm.DB
}

func NewUserRepository(db *gorm.DB) repository.UserRepositoryInterface {
	return UserRepository{
		DB: db,
	}
}

func (r UserRepository) Insert(u *model.User) error {
	return r.DB.Create(&u).Error
}

func (r UserRepository) IsUnique(u *model.User) (bool, error) {
	type exists struct {
		Result bool
	}

	var e exists
	if err := r.DB.
		Raw("SELECT EXISTS(SELECT * FROM users WHERE email = ?) AS result", u.Email).
		Scan(&e).Error; err != nil {
		return e.Result, err
	}

	return !e.Result, nil
}

func (r UserRepository) FindOne(id string) (*model.User, error) {
	user := &model.User{}
	if err := r.DB.Where("ID = ?", id).First(user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func (r UserRepository) FindOneByEmail(email string) (*model.User, error) {
	user := &model.User{}
	if err := r.DB.Where("Email = ?", email).First(user).Error; err != nil {
		return nil, err
	}

	return user, nil
}
