package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/hopper/model"
	"gitlab.com/zenpagos/hopper/repository"
)

type ApiKeyRepository struct {
	DB *gorm.DB
}

func NewApiKeyRepository(db *gorm.DB) repository.ApiKeyRepositoryInterface {
	return ApiKeyRepository{
		DB: db,
	}
}

func (r ApiKeyRepository) Insert(u *model.ApiKey) error {
	return r.DB.Create(&u).Error
}

func (r ApiKeyRepository) FindOneByToken(t string) (*model.ApiKey, error) {
	apiKey := &model.ApiKey{}
	if err := r.DB.Where("Token = ?", t).First(apiKey).Error; err != nil {
		return nil, err
	}

	return apiKey, nil
}

func (r ApiKeyRepository) FindByAccountID(accountID uuid.UUID) ([]model.ApiKey, error) {
	var aks []model.ApiKey
	if err := r.DB.Where("account_id = ?", accountID.String()).
		Find(&aks).Error; err != nil {
		return nil, err
	}

	return aks, nil
}
