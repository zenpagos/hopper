// Package repository and it's sub-package represents data persistence service, mainly access database,
// but also including data persisted by other Micro-service.
// For Micro-server, only the interface is defined in this package, the data transformation code is in adapter package
// This is the top level package and it only defines interface, and all implementations are defined in sub-package
// Use case package depends on it.
package repository

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/hopper/model"
)

type UserRepositoryInterface interface {
	Insert(user *model.User) error
	IsUnique(user *model.User) (bool, error)
	FindOne(id string) (*model.User, error)
	FindOneByEmail(email string) (*model.User, error)
}

type ApiKeyRepositoryInterface interface {
	Insert(apiKey *model.ApiKey) error
	FindOneByToken(token string) (*model.ApiKey, error)
	FindByAccountID(accountID uuid.UUID) ([]model.ApiKey, error)
}
