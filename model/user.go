// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	uuid "github.com/satori/go.uuid"
)

type User struct {
	ID                uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt         int64     `sql:"type:int;not null"`
	UpdatedAt         *int64    `sql:"type:int"`
	FirstName         string    `sql:"type:varchar(255);not null"`
	LastName          string    `sql:"type:varchar(255);not null"`
	Email             string    `sql:"type:varchar(255);not null"`
	Password          string    `sql:"type:varchar(255);not null"`
	MobilePhoneNumber string    `sql:"type:varchar(255)"`
}

func (u User) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.FirstName, validation.Required),
		validation.Field(&u.LastName, validation.Required),
		validation.Field(&u.Email, is.Email, validation.Required),
		validation.Field(&u.Password, validation.Required),
	)
}
