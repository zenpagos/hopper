package schema

import "fmt"

type UserRegisteredSchema struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

func (s UserRegisteredSchema) FullName() string {
	return fmt.Sprintf("%s %s", s.FirstName, s.LastName)
}
